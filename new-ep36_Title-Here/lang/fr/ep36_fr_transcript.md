# Transcript of Pepper&Carrot Episode 36 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 36: L'attaque Surprise

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|DES EXCUSES ?!
Wasabi|2|False|Mais de qui tu te moques ?!
Wasabi|3|False|JETEZ-MOI ÇA EN PRISON TOUT DE SUITE !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Psch h...
Pepper|2|True|Zut !
Pepper|3|False|Je ne peux rien faire ici !
Pepper|4|False|Fichue prison magique ! Grrrr !
Son|5|False|CLING !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Mais qu'est-ce que j'ai pu être naïve !
Shichimi|2|True|Chhhhh Pepper !
Shichimi|3|False|Fais moins de bruit.
Pepper|4|False|Qui va là ?!
Shichimi|5|True|Chhhhh ! J't'ai dit !
Shichimi|6|True|Approche.
Shichimi|7|False|Je suis venue te libérer.
Son|8|False|Dzii...
Pepper|9|False|Shichimi ?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Je suis... je suis désolée pour ce qui s'est passé...
Shichimi|2|True|...tu sais, pour notre combat.
Shichimi|3|True|Je...
Shichimi|4|False|Je devais le faire.
Pepper|5|True|Ne t'inquiète pas, j'avais compris.
Pepper|6|False|Merci d'être venue.
Shichimi|7|False|Cette cellule magique est vraiment puissante, ils ont mis le paquet pour toi.
Pepper|8|False|Ha, ha !
Shichimi|9|False|Moins fort, tu vas nous faire repérer.
Rat|10|True|LAP
Rat|11|True|LAP
Rat|12|False|LAP
Shichimi|13|True|Tu sais,
Shichimi|14|True|je suis aussi là car, juste après la cérémonie, j'ai pu intégrer le cercle de confiance de Wasabi.
Shichimi|15|False|Et ainsi, connaître ses plans...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|CLING !
Son|2|True|CLING !
Son|3|True|CLING !
Son|4|True|CLING !
Son|5|False|CLING !
Shichimi|6|True|C'est terrible, Pepper.
Shichimi|7|True|Wasabi veut dominer , purement et simplement, toutes les autres écoles de magie ...
Shichimi|8|False|Elle partira demain à l'aube avec une armée de sorcières pour Qualicity...
Pepper|9|True|Oh non !
Pepper|10|True|Coriandre.
Pepper|11|False|Et son royaume !
Shichimi|12|True|Et la magie Zombiah !
Shichimi|13|True|Nous devons la prévenir au plus vite.
Shichimi|14|False|Une pilote et un dragon nous attendent sur la plateforme afin de partir d'ici.
Shichimi|15|False|Ça y est, la serrure a fini par céder !
Son|16|False|Dzing !
Pepper|17|True|Bravo !
Pepper|18|False|Je file chercher Carrot et mon chapeau.
Rat|19|False|scouiiik
Carrot|20|False|K K K KSSS !
Rat|21|False|skiiiiiiiiiik !
Pepper|22|False|! ! !
Shichimi|23|False|! ! !
Guarde|24|True|GARDE !
Guarde|25|True|ALERTE !
Guarde|26|False|UN INTRUS ESSAIE DE FAIRE ÉVADER UNE PRISONNIÈRE !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|On était si proches du but...
Pepper|2|False|Oui, si proches...
Pepper|3|False|Au fait, sais-tu pourquoi Wasabi en a tant après moi ?
Shichimi|4|True|Elle a peur des sorcières de Chaosah, Pepper...
Shichimi|5|True|Et surtout de vos réactions en chaîne.
Shichimi|6|False|C'est une menace majeure pour ses plans d'après elle.
Pepper|7|True|Ah, ça, pfff...
Pepper|8|False|Elle n'a pas à s'en faire : j'en ai encore jamais réussi une seule.
Shichimi|9|False|Vraiment ?
Pepper|10|False|Oui vraiment, hahaha !
Shichimi|11|True|Hihihi !
Shichimi|12|False|Elle est vraiment parano...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|True|Officier !
Roi|2|False|Confirmez-vous que ceci est bien le temple de cette sorcière ?
Officier|3|True|Hautement probable, mon roi !
Officier|4|False|Plusieurs de nos informateurs l'ont aperçue ici récemment.
Roi|5|True|GRrrr...
Roi|6|False|C'est donc là que vit cette menace qui nuit à notre art de la guerre et nos traditions !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|False|En représailles, célébrons notre alliance en rayant ce temple de la surface d'Hereva.
Son|2|False|Tap
Ennemi|3|False|B ien dit !
Armée|4|True|Ouais !
Armée|5|True|Yâaa !
Armée|6|True|Yaaa !
Armée|7|True|Ouaiiiis !
Armée|8|True|Yihaaa !
Armée|9|True|Yuurr !
Armée|10|True|Yihaaa !
Armée|11|True|Yihaaa !
Armée|12|True|Ouais !
Armée|13|False|Houra !
Roi|14|True|CATAPULTES !
Roi|15|False|FEUUUU !!!
Son|16|False|hôoWOUUUUUUUUUU !
Son|17|True|Schwisss !
Son|18|False|Schwisss !
Roi|19|False|À L'ASSAUUUT !!!
Pepper|20|True|Que se passe-t'il ?
Pepper|21|False|Une attaque ?!
Son|22|True|B OU M !
Son|23|False|B O OU M !
Shichimi|24|False|Quoi ?!
Son|25|True|BA~D OU M !
Son|26|False|B OU M !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|KOF
Pepper|2|False|KOF ! !
Pepper|3|False|Shichimi ! Tout va bien ?
Shichimi|4|True|Oui, rien de cassé !
Shichimi|5|True|Et toi ?
Shichimi|6|False|Carrot ?
Pepper|7|False|On va bien.
Pepper|8|True|Non mais ça alors...
Pepper|9|False|J'y... crois... pas...
Shichimi|10|True|D'où viennent ces armées ?!
Shichimi|11|True|Des catapultes ?!
Shichimi|12|False|Qu'est-ce qu'ils nous veulent ?!
Pepper|13|True|Je ne sais pas...
Pepper|14|False|Mais je connais ces deux-là.
Shichimi|15|False|?
Shichimi|16|False|! ! !
Shichimi|17|False|Torreya, par ici !

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi !
Torreya|2|False|Les esprits soient loués ! Tu es saine et sauve.
Son|3|False|Tap
Torreya|4|True|J'étais si inquiète quand j'ai su qu'ils t'avaient emprisonnée !
Torreya|5|True|Et cette bataille !
Torreya|6|False|Quel chaos !
Shichimi|7|False|Torreya, c'est si bon te voir.
Pepper|8|True|Mais ça alors...
Pepper|9|True|Cette pilote de dragon était donc la copine de Shichimi...
Pepper|10|True|Je n'imagine pas les conséquences si je m'étais débarrassée d'elle.
Pepper|11|True|Et ces armées, elles ont certainement dû me suivre.
Pepper|12|True|Sans elles, on ne serait pas libres.
Pepper|13|False|Tout ça semble si connecté ...
Pepper|14|False|...OH !

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Qu'est-ce qu'elle a ?
Shichimi|2|False|Pepper ? Tout va bien ?
Pepper|3|True|Oui, ça va.
Pepper|4|False|C'est juste que je viens d'avoir un déclic.
Pepper|5|True|Toutes ces conséquences, directes ou indirectes, ce sont mes actions, mes choix...
Pepper|6|False|...en quelque sorte : ma réaction en chaîne !
Shichimi|7|True|Vraiment ?
Shichimi|8|False|Il faudra que tu m'expliques.
Torreya|9|True|Trêve de bavardage, on est en plein champ de bataille.
Torreya|10|True|On discutera de tout ça en vol.
Torreya|11|False|Monte, Pepper !
Shichimi|12|True|Torreya a raison.
Shichimi|13|False|Dans tous les cas, on doit rejoindre Qualicity.
Pepper|14|False|Non, attendez.
Pepper|15|True|L'armée de Wasabi s'apprête à riposter.
Pepper|16|True|On ne peut pas les laisser s'entretuer comme ça.
Pepper|17|False|Je sens que c'est ma responsabilité de mettre un terme à cette bataille.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Mais comment ?
Arra|2|True|Oui, comment comptes-tu faire, sorcière ?
Arra|3|False|Tu n'as pas récupéré beaucoup de Réa, je peux le ressentir.
Pepper|4|True|Tu as parfaitement raison, mais j'ai un sort en stock qui pourrait tout arranger.
Pepper|5|False|J'aurai seulement besoin de ton Réa pour pouvoir atteindre tout ce monde-là.
Arra|6|True|Donner de l'énergie à une sorcière ?
Arra|7|True|C'est interdit !
Arra|8|False|OUBLIE ! JAMAIS !
Pepper|9|False|Tu préfères donc laisser faire ce massacre ?
Torreya|10|True|S'il te plait, Arra. Fais une exception. Ces filles et ces dragons qui se battent, ça reste notre école, notre famille.
Torreya|11|False|Même pour toi.
Shichimi|12|False|Oui, s'il te plait, Arra.
Arra|13|True|PFF ! Bien !
Arra|14|False|Mais à ses risques et périls !

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|fs h h h h ! !
Pepper|2|False|WOOHAAAA !
Pepper|3|False|C'est donc ça, le Réa d'un dragon !
Shichimi|4|False|Pepper, vite ! Le temps presse !
Pepper|5|True|Allus... !
Pepper|6|True|Yous... !
Pepper|7|True|Needus... !
Pepper|8|True|Is... !
Pepper|9|False|...LOVUS !
Son|10|False|Dzziooo ! !

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|Fiizz !
Son|2|True|Dziing !
Son|3|True|Ffhii !
Son|4|True|Schii !
Son|5|True|Schsss !
Son|6|True|Fiizz !
Son|7|True|Dziing !
Son|8|True|Schii !
Son|9|True|Ffhii !
Son|10|False|Schsss !
Pepper|11|True|Ce sort était mon premier essai quand j'ai travaillé aux sorts anti-guerres.
Pepper|12|True|Celui-ci change les ennemis en amis.
Pepper|13|False|La violence en amour et compassion.
Shichimi|14|True|Wouahou !
Shichimi|15|True|Mais... mais c'est génial Pepper !
Shichimi|16|False|Ils arrêtent de se battre !
Torreya|17|False|Ça marche !

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Mais qu'est-ce qu'il se passe ?
Shichimi|2|False|Certains commencent à s'embrasser... ?!
Torreya|3|True|Heu... ça fait un paquet de nouveaux couples ça !
Torreya|4|False|C'était prévu, Pepper ?
Pepper|5|False|Oh non ! Je crois que c'est le Réa du dragon qui a surdosé l'amour dans mon sortilège.
Torreya|6|False|Haha, cette bataille va entrer directement dans les manuels d'histoire !
Shichimi|7|True|Hi, hi !
Shichimi|8|False|Certainement !
Pepper|9|False|Rhôo, la honte !
Écriture|10|False|- FIN -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Le 15 Décembre 2021 Art & scénario : David Revoy. Lecteurs de la version bêta : Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini , Valvin. Version française originale Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 5.0beta, Inkscape 1.1 on Kubuntu Linux 20.04 Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Le saviez-vous ?
Pepper|3|False|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de %%%% mécènes !
Pepper|5|False|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|6|False|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|7|False|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|8|False|Merci !
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
