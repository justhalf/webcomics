# Transcript of Pepper&Carrot Episode 28 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 28a: La Festoj

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rakontanto|1|True|La tri lunoj de Hereva estis linie tiun nokton,
Rakontanto|2|False|kaj ilia reflektita lumo kaŭzis veran spektaklon en la katedralo de Zombiaho.
Rakontanto|3|False|Okazis sub tiu magia lumo, ke Koriandro, mia amiko, iĝis...
Rakontanto|4|False|Reĝino de Kvalicito.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rakontanto|1|False|La festoj komenciĝis tuj poste.
Rakontanto|2|False|Festego kun ĉiuj la magiaj lernejoj kaj centoj da prestiĝaj gastoj el la tuta Herevo.
Pipro|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Ho! Estas vi!
Pipro|3|False|Mi estis profunde en miaj pensoj.
Karoĉjo|2|False|rub rub
Pipro|4|False|Mi eniros nun. Malvarmiĝas ĉi tie.
Ĵurnalisto|6|False|Rapidu! Ŝi estas ĉi tie!
Ĵurnalisto|5|False|Rapide! Estas ŝi!
Ĵurnalisto|7|False|Sinjorino Safrano! Ĉu vi povas diri kelkajn vortojn por la Kvalicita Ĵurnalo?
Safrano|8|False|Jes, kompreneble!
Pipro|15|False|...
Pipro|16|False|Ĉu vi vidas, Karoĉjo? Mi pensas, ke mi komprenas, kial mi ne estas ĝoja.
Sono|9|False|F LAŜ|nowhitespace
Ĵurnalisto|13|True|Sinjorino Safrano! Hereva Revuo de Stilo.
Sono|10|False|F LAŜ|nowhitespace
Sono|11|False|F LAŜ|nowhitespace
Sono|12|False|F LAŜ|nowhitespace
Ĵurnalisto|14|False|De kiu fasonisto vi surhavas ĉi tiun nokton?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Ĉiuj miaj amikoj estas tre sukcesaj kaj kelkfoje mi dezirus, ke mi havu nur parton de kiu ili havas.
Pipro|2|False|Koriandro estas reĝino.
Pipro|3|False|Safrano estas famulino.
Pipro|4|False|Eĉ Ŝiŝimio aspektas tre perfekta kun sia lernejo.
Pipro|5|False|Kaj mi? Kion mi havas?
Pipro|6|False|Ĉi tion.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ŝiŝimio|1|False|Pipro?
Ŝiŝimio|2|False|Mi kaŝe prenis iom da manĝaĵo. Ĉu vi volas ion?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ŝiŝimio|1|True|Ŝajnas stranga, ke mi devas kaŝiri por manĝi iom da manĝaĵo.
Ŝiŝimio|2|False|Sed nia instruisto diras, ke ni devas aspekti kiel puraj spiritoj sen bazaj homaj bezonoj.
Safrano|3|False|Ha! Tie vi estas!
Safrano|4|True|Finfine! Ie, kien mi povas iri por eskapi de fotistoj.
Safrano|5|False|Ili frenezigas min.
Koriandro|6|False|Fotistoj?
Koriandro|7|False|Penu eviti enuajn diskutojn kun politikistoj, kiam vi havas ĉi tion sur via kapo.
Sono|8|False|grat grat

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandro|1|True|Parenteze, Pipro, mi fine rekontis viajn baptopatrinojn.
Koriandro|2|False|Ili vere estas “specialaj”.
Pipro|3|False|!!!
Koriandro|4|True|Mi volas diri, ke vi estas tre bonŝanca.
Koriandro|5|False|Mi certas, ke ili lasas al vi fari kion ajn vi volas.
Koriandro|6|False|Kiel, defii ĉi tiun neutilan pozadon kaj babiladi sen zorgi pri diplomateco.
Safrano|7|False|Aŭ danci kaj ĝoji sen zorgi pri tio, kion la homoj povus pensi.
Ŝiŝimio|8|False|Aŭ manĝi la tutan manĝaĵon de la bufedo sen timi, ke iu vidu vin!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rakontanto|4|False|DAŬRIGOTA...
Ŝiŝimio|1|False|Ho! Pipro!? Ĉu ni diris ion, kion afliktis vin?
Pipro|2|True|Tute ne!
Pipro|3|False|Dankon al vi, miaj amikoj!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|5|True|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon ĉi tie!
Pipro|3|True|Pepper&Carrot estas tute libera, malfermitkoda kaj subtenita danke al la mecenateco de siaj legantoj.
Pipro|4|False|Pri ĉi tiu rakonto, dankon al la 960 mecenantoj!
Pipro|7|True|Vidu www.peppercarrot.com por pli da informo!
Pipro|6|True|Ni estas en Patreon, Tipeee, PayPal, Liberapay ...kaj en multaj pli!
Pipro|8|False|Dankon!
Pipro|2|True|Ĉu vi sciis?
Atribuintaro|1|False|Januaro, 2019 Arto kaj scenaro: David Revoy. Beta-legantoj: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Esperanta versio Traduko: Jorge Maldonado Ventura. Bazita sur la universo de Hereva Kreinto: David Revoy. Ĉefa fleganto: Craig Maloney. Verkistoj: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korektistoj: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Programaro: Krita 4.1.5~appimage, Inkscape 0.92.3 sur Kubuntu 18.04.1. Licenco: Krea Komunaĵo Atribuite 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
