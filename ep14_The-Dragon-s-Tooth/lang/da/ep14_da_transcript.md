# Transcript of Pepper&Carrot Episode 14 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 14: Dragetanden

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spidskommen|5|True|Nå, nå, nå! Så er ferien slut!
Spidskommen|6|False|Lad os starte med et kursus i oldtidens eliksirer og deres primære ingredienser
Spidskommen|7|True|Hmm… Pokkers! Der er ikke mere dragetandspulver
Spidskommen|8|False|Det nytter ikke at starte uden denne ingrediens...
Skrift|9|False|Dragetand
Skrift|4|False|33
Skrift|3|False|PRIVAT
Skrift|2|True|HEKSEN
Skrift|1|True|PAS PÅ

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spidskommen|1|False|I denne kulde?! Nej tak!
Cayenne|2|False|Mmm... Enig...
Timian|3|False|Ingen problemer. Det tager jeg mig af!
Pepper|4|False|Jeg er straks tilbage!
Pepper|6|False|"Kulden", "kulden"!
Pepper|7|True|Sikke en flok bangebukse!
Pepper|8|False|En god tang, noget beskyttelsesudstyr og så er dragertænderne vores!
Pepper|9|False|Pepper vent!
Spidskommen|5|False|Cayenne! Timian! Kan i gå ud at hente noget dragetand?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ha ha!
Pepper|2|False|Hold da op! Det bliver vist lidt sværere end jeg troede!
Pepper|3|True|Pfff!
Pepper|4|False|… og jeg som troede det ville være nemmere med en vinddrage!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carrot!...
Pepper|2|True|Kom tilbage...
Pepper|3|True|... Sumpdragen er kendt for at være mere venlig af natur
Pepper|4|False|Det er ikke noget, jeg har fundet på...
Pepper|5|True|Hmm... Når det kommer til lyndragen...
Pepper|6|False|... så er jeg ikke sikker på, at det er det rette værktøj

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ok.
Pepper|2|True|Det er for svært...
Pepper|3|False|... jeg giver op.
Pepper|5|False|Pepper giver aldrig op!
Pepper|4|True|Nej!
Fugl|7|False|Næste morgen|nowhitespace
Fortæller|6|False|! !
Spidskommen|8|False|KYKKELIKY YYYYY ! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|9|False|- Slut -
Credits|10|False|01/2016 – Tegning og manuskript: David Revoy – Oversættelse: Emmiline, Alexandre, og Rikke Alapetite
Skrift|4|True|Drage-|nowhitespace
Skrift|5|True|Tandlæge
Skrift|6|False|Gratis!
Lyd|3|False|pop!
Pepper|1|False|Nå, imponeret, hva'?
Pepper|2|False|Jeg har mindst hundrede dragetænder!
Spidskommen|7|True|... men Pepper, dragetanden...
Spidskommen|8|False|... er en urt!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 671 tilhængere:
Credits|2|True|Licens: Creative Commons Kreditering 4.0 Kildematerialet: tilgængelige på www.peppercarrot.com Værktøj: denne episode er 100% designet med fri software: Krita 2.9.10, Inkscape 0.91 på Linux Mint 17
Credits|3|False|Støt næste episode af Pepper&Carrot; hver en krone gør en forskel
Credits|4|False|https://www.patreon.com/davidrevoy
