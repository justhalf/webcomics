# Transcript of Pepper&Carrot Episode 25 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 25: There Are No Shortcuts

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|CAT FOOD

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|13|False|- FIN -
Pepper|1|True|?
Pepper|2|True|?
Pepper|3|True|?
Pepper|4|False|?
Pepper|5|True|?
Pepper|6|True|?
Pepper|7|True|?
Pepper|8|False|?
Pepper|9|True|?
Pepper|10|True|?
Pepper|11|True|?
Pepper|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|7|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
Credits|6|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 909 Patrons:
Credits|1|False|05/2018 - www.peppercarrot.com - Art & Scenario: David Revoy
Credits|3|False|Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|4|False|Software: Krita 4.0.0, Inkscape 0.92.3 on Kubuntu 17.10
Credits|5|False|Licence: Creative Commons Attribution 4.0
Credits|2|False|Beta feedback: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire and Zveryok.
