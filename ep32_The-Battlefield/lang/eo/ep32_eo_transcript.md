# Transcript of Pepper&Carrot Episode 32 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 32a: La Batalejo

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Reĝo|1|True|Oficiro!
Reĝo|2|False|Ĉu vi rekrutis sorĉistinon kiel mi petis?
Oficiro|3|True|Jes, mia estro!
Oficiro|4|False|Ŝi staras apud vi.
Reĝo|5|False|...?
Pipro|6|True|Sal!
Pipro|7|False|Mia nomo estas Pip...
Reĝo|8|False|?!!
Reĝo|9|True|STULTULO!!!
Reĝo|10|True|Kial vi varbis ĉi tiun infanon?!
Reĝo|11|False|Mi bezonas veran batalejan sorĉistinon!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Pardonu min!
Pipro|2|True|Mi estas vera Ĥaosaha Sorĉistino.
Pipro|3|False|Eĉ mi havas diplomon, kiu diras...
Writing|4|True|Diplomo
Writing|5|True|de
Writing|6|False|Ĥaosaho
Writing|8|True|Kajeno
Writing|9|False|Kumino
Writing|7|True|Timiano|nowhitespace
Writing|10|False|~ al Pipro ~
Reĝo|11|False|SILENTU!
Reĝo|12|True|Mi ne havas bezonon de infanoj en ĉi tiu armeo.
Reĝo|13|False|Iru hejmen kaj ludu kun viaj pupoj.
Sono|14|False|Klak!
Army|15|True|HAHA HA HA!
Army|16|True|HAHA HA HA!
Army|17|True|HAHA HA HA!
Army|18|False|HA HA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Mi ne kredas tion!
Pipro|2|False|Mi lernis dum jaroj, sed neniu konsideras min serioza, ĉar...
Pipro|3|False|...mi ne aspektas sufiĉe sperta!
Sono|4|False|PUŬF ! !|nowhitespace
Pipro|5|False|KAROĈJO !|nowhitespace
Sono|6|False|PAF ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Vere, Karoĉjo?
Pipro|2|True|Mi esperas, ke tiu manĝo valoris la penon.
Pipro|3|False|Vi aspektas malbelega...
Pipro|4|False|...Vi aspektas...
Pipro|5|True|La aspekto!
Pipro|6|False|Kompreneble!
Sono|7|False|Krak !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|HE!
Pipro|2|False|Mi aŭdis, ke vi serĉas VERAN SORĈISTINON?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Reĝo|1|True|Tamen pripensinte, revoku tiun infanon.
Reĝo|2|False|Ĉi tiu verŝajne kostas tro.
Writing|3|False|DAŬRIGOTA…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|5|True|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon ĉi tie!
Pipro|3|True|Pepper&Carrot estas tute libera, malfermitkoda kaj subtenita danke al la mecenateco de siaj legantoj.
Pipro|4|False|Pri ĉi tiu rakonto, dankon al la 1121 mecenantoj!
Pipro|7|True|Vidu www.peppercarrot.com por pli da informo!
Pipro|6|True|Ni estas en Patreon, Tipeee, PayPal, Liberapay ...kaj en multaj pli!
Pipro|8|False|Dankon!
Pipro|2|True|Ĉu vi sciis?
Atribuintaro|1|False|31a de Marto de 2020 Arto kaj scenaro: David Revoy. Beta-legantoj: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Esperanta version Traduko: Jorge Maldonado Ventura . Bazita sur la universo de Hereva Kreinto: David Revoy. Ĉefa fleganto: Craig Maloney. Verkistoj: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korektistoj: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Programaro: Krita 4.2.9-beta, Inkscape 0.92.3 sur Kubuntu 19.10. Licenco: Krea Komunaĵo Atribuite 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
